package net.indierising.momentum;

import org.newdawn.slick.AppGameContainer;
import org.newdawn.slick.BasicGame;
import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.SlickException;

public class Game extends BasicGame {
	public Game(String title) {
		super(title);
	}
	
	public void init(GameContainer gc) throws SlickException {

	}
	
	public void render(GameContainer gc, Graphics g) throws SlickException {

	}
	
	public void update(GameContainer gc, int delta) throws SlickException {

	}
	
	public static AppGameContainer appgc;
	public static void main(String[] args) {
		try {
			appgc = new AppGameContainer(new Game("Momentum"));
			appgc.setDisplayMode(640, 480, false);
			appgc.setTargetFrameRate(60);
			appgc.setAlwaysRender(true);
			appgc.setUpdateOnlyWhenVisible(false);
			appgc.start();
		} catch (SlickException e) {
			e.printStackTrace();
		}
	}
}
